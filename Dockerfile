FROM alpine:latest

MAINTAINER Nguyễn Hồng Quân <ng.hong.quan@gmail.com>

ENV LANG en_US.UTF-8

RUN apk --update add ninja meson musl-dev gcc && rm -rf /var/cache/apk/*

